import * as path from 'path'
import * as cdk from 'aws-cdk-lib'
import { Cors, LambdaIntegration, RestApi } from 'aws-cdk-lib/aws-apigateway'
import { Certificate } from 'aws-cdk-lib/aws-certificatemanager'
import { AllowedMethods, Distribution, OriginAccessIdentity } from 'aws-cdk-lib/aws-cloudfront'
import { S3Origin } from 'aws-cdk-lib/aws-cloudfront-origins'
import { ManagedPolicy, Role, ServicePrincipal } from 'aws-cdk-lib/aws-iam'
import { Code, Function, Runtime } from 'aws-cdk-lib/aws-lambda'
import { Bucket, BucketAccessControl } from 'aws-cdk-lib/aws-s3'
import { BucketDeployment, Source } from 'aws-cdk-lib/aws-s3-deployment'
import { Construct } from 'constructs'
import { AttributeType, Billing, TableV2 } from 'aws-cdk-lib/aws-dynamodb'

export class ApiStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props)

    const bucket = new Bucket(this, 'Bucket', {
      bucketName: 'text-me-when-you-get-there',
      accessControl: BucketAccessControl.PRIVATE
    })

    const originAccessIdentity = new OriginAccessIdentity(this, 'OriginAccessIdentity')
    bucket.grantRead(originAccessIdentity)

    const certificate = Certificate.fromCertificateArn(this, 'Certificate',
      'arn:aws:acm:us-east-1:704733417709:certificate/730bc6d7-0801-47e5-a48a-6e2cfbff5478')

    const cf = new Distribution(this, 'Distribution', {
      defaultRootObject: 'index.html',
      domainNames: ['textmewhenyougetthere.org'],
      certificate,
      defaultBehavior: {
        origin: new S3Origin(bucket, { originAccessIdentity }),
        allowedMethods: AllowedMethods.ALLOW_GET_HEAD_OPTIONS
      },
      errorResponses: [
        {
          httpStatus: 404,
          responseHttpStatus: 200,
          responsePagePath: '/index.html',
          ttl: cdk.Duration.minutes(30)
        }
      ]
    })

    new BucketDeployment(this, 'BucketDeployment', {
      destinationBucket: bucket,
      sources: [Source.asset(path.resolve(__dirname, '../../reactapp/build'))],
      distribution: cf,
      distributionPaths: ['/*']
    })

    const bucketAccessRole = new Role(this, 'TMWYGTBucketAccessRole', {
      assumedBy: new ServicePrincipal('lambda.amazonaws.com'),
      roleName: 'TMWYGTBucketAccessRole',
      description: 'Role used to read/write location info',
      managedPolicies: [
        ManagedPolicy.fromAwsManagedPolicyName('AmazonDynamoDBFullAccess'),
        ManagedPolicy.fromAwsManagedPolicyName('AWSLambdaExecute')
      ]
    })

    new TableV2(this, 'LocationTable', {
      sortKey: {
        name: 'mapId',
        type: AttributeType.STRING
      },
      partitionKey: {
        name: 'name',
        type: AttributeType.STRING
      },
      globalSecondaryIndexes: [{
        indexName: 'maps',
        partitionKey: {
          name: 'mapId',
          type: AttributeType.STRING
        }
      }],
      billing: Billing.onDemand(),
      tableName: 'TMWYGTLocations',
      timeToLiveAttribute: 'expires',
      removalPolicy: cdk.RemovalPolicy.DESTROY
    })

    const codePath = '../api/dist'

    const api = new RestApi(this, 'TMWYGTAPI', {
      restApiName: 'TMWYGT API',
      defaultCorsPreflightOptions: {
        allowOrigins: Cors.ALL_ORIGINS,
        allowHeaders: Cors.DEFAULT_HEADERS,
        allowMethods: Cors.ALL_METHODS
      }
    })

    const functionEnvironment = {}

    const tmwygtFunc = new Function(this, 'TMWYGTFunc', {
      code: Code.fromAsset(codePath),
      functionName: 'TMWYGT',
      handler: 'TMWYGT.handler',
      role: bucketAccessRole,
      memorySize: 512,
      runtime: Runtime.NODEJS_18_X,
      timeout: cdk.Duration.seconds(29),
      environment: functionEnvironment
    })

    api.root.resourceForPath('api/{mapId}').addMethod('POST', new LambdaIntegration(tmwygtFunc))
  }
}
