import {
  DynamoDBClient, PutItemCommand, QueryCommand
} from '@aws-sdk/client-dynamodb'
import {
  APIGatewayProxyEvent,
  APIGatewayProxyResult,
  Handler
} from 'aws-lambda'

const TABLE_NAME = 'TMWYGTLocations'

export const handler: Handler = async (event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> => {
  if (!event.body || !event.pathParameters?.mapId) {
    return { statusCode: 400, body: 'no data provided' }
  }
  const { name, lat, lon } = JSON.parse(event.body)
  const mapId = event.pathParameters.mapId
  console.log(mapId, name, lat, lon)

  const ddbClient = new DynamoDBClient({ region: 'us-west-2' })
  await ddbClient.send(new PutItemCommand({
    TableName: TABLE_NAME,
    Item: {
      mapId: { S: mapId },
      name: { S: name },
      lat: { S: lat.toString() },
      lon: { S: lon.toString() },
      updated: { S: new Date().getTime().toString() },
      expires: { S: Math.floor(new Date().getTime() + 60).toString() }
    }
  }))
  const resp = await ddbClient.send(new QueryCommand({
    TableName: TABLE_NAME,
    KeyConditionExpression: 'mapId = :mapId',
    IndexName: 'maps',
    ExpressionAttributeValues: {
      ':mapId': {
        S: mapId
      }
    }
  }))
  const locations = resp.Items!.map(item => (
    {
      name: item.name.S,
      lat: parseFloat(item.lat.S!),
      lon: parseFloat(item.lon.S!)
    }
  ))

  return {
    headers: {
      'access-control-allow-headers':
        'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token,X-Amz-User-Agent',
      'access-control-allow-methods': 'OPTIONS,GET,PUT,POST,DELETE,PATCH,HEAD',
      'access-control-allow-origin': '*'
    },
    statusCode: 200,
    body: JSON.stringify(locations)
  }
}
