# Text Me When You Get There
An application inspired by my own forgetfulness to inform my partner that I've arrived safely at my destination.

![Architecture Diagram](tmwygt.png)

## Development
There are three sub-packages within the repo. Their directories are
- `reactapp`: the web app component
- `api`: the REST API hosted by AWS
- `infra`: the cdk stack used to deploy the above

After cloning the repository.

[Deploy](#deployment) the API and set the `proxy` field in `reactapp/package.json` to point to your API endpoint (e.g. `https://gs3s2a2vr8.execute-api.us-west-2.amazonaws.com/prod`).

```
cd reactapp
npm i
npm start
```

## Deployment
It is possible to deploy only the API, only the React App, or both using one of the following `npm` commands

```
cd infra
npm run deploy:api  # API only
npm run deploy:reactapp   # React App only
npm run deploy    # both
```

## TODO
Seeing as how geolocation is currently [not available in the background](https://github.com/w3c/geolocation-sensor/issues/22) for web apps, a set of native mobile apps is required to make this fully functional and in any way a worthwhile endeavor. I detest mobile apps but maybe one day I'll look into this.
